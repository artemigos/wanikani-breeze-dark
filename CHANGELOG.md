# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [Unreleased]

*no notable changes*


## [1.3.6] - 2020-03-10

### Fixed
- Word spacing in lesson quiz


## [1.3.5] - 2020-03-10

### Fixed
- Sitemap background color is now dark
- Text color on settings pages are now lighter
- Links in nav menu no longer use default link styling
- Adjusted dashboard style for new layout


## [1.3.4] - 2020-01-05

### Added
- Styling for new progress bar

### Changed
- Navbar now has its old shadow back

### Fixed
- Progress entries now have correct text color


## [1.3.3] - 2020-01-02

### Changed
- Use GitLab pages for automatic CSS build

### Fixed
- Global header background color


## [1.3.2] - 2018-12-08

### Fixed
- Alert message styling


## [1.3.1] - 2018-10-27

### Fixed
- Table width for PSC userscript


## [1.3.0] - 2018-10-27

### Added
- Support for Phon. Sem. Composition userscript


## [1.2.4] - 2018-10-27

### Fixed
- Batch item tab within lessons


## [1.2.3] - 2018-10-27

### Added
- Support for "Vocabulary for Wanikani" userscript


## [1.2.2] - 2018-06-07

### Changed
- Tweak style for Ultimate Timeline userscript

### Fixed
- Character list and grid styling


## [1.2.1] - 2018-05-22

### Changed
- Update style for Ultimate Timeline UserScript

### Fixed
- WaniKani Ultimate Timeline popup background


## [1.2.0] - 2018-05-15

### Added
- Partial styling for WKOF
- Support for WakiKani Leech Training userscript

### Fixed
- Position progress bar for leech trainer
- Make some dropdown-menu styles more specific


## [1.1.0] - 2017-12-03

### Added
- Usercss for Stylus
- Support for Stroke Order Diagram userscript
- Config option for burned color
- WK Anti-burnout userscript styling
- Style icons and time in alert messages
- Styling for newbie checklist

### Changed
- Update WaniKani lesson cap userscript style
- Update lesson cap userscript styles
- Update alert style
- Update dashboard footer

### Fixed
- Only display footer image on dashboard
- Alert message on reviews page
- Footer image positioning
- Highlight text color on lessons


## [1.0.0] - 2017-07-16

### Added
- Apply link styling
- Support for override script
- Styling for login/signup page
- Background color options for reading/meaning in reviews
- Support for custom Radical/Kanji/Vocabulary colors
- Support for the WaniKani Dashboard Progress Plus userscript
- Support for the WaniKani Reorder Ultimate 2 userscript

### Removed
- General radical id selector
- Text shadows from tables on dashboard

### Changed
- Make the character background in reviews changeable
- Give some character items a more "iconic" look
- Make sure "dangerous" buttons are marked
- Make background for progress bar darker
- Add darker header to item lists and forums on dashboard and in review/lesson summary
- Animations and shadows for button and input field
- Adjust spacing and coloring for Ultimate Timeline graph
- Make info popups on reviews summary page better readable

### Fixed
- Settings page styling to work with new layout
- Styling for related items in lesson quiz
- Layering for lesson quiz sheets
- Styling for lesson/quiz ready screens
- Styling for textarea
- Coloring of user synonym input
- Position of batch items in lessons (bottom list)
- Colors for lessons
- Colors for total item numbers on review summary
- Spacing between subsections on dashboard
- Styling for Reorder Ultimate userscript in lessons
- Correct/incorrect highlighting in reviews


## [0.6.0] - 2016-12-24

*no notable changes*


## [0.1.0] - 2016-11-24

*no notable changes*


<!-- links -->

[Unreleased]: https://gitlab.com/valeth/wanikani-breeze-dark/tree/master
[1.3.6]: https://gitlab.com/valeth/wanikani-breeze-dark/tree/1.3.6
[1.3.5]: https://gitlab.com/valeth/wanikani-breeze-dark/tree/1.3.5
[1.3.4]: https://gitlab.com/valeth/wanikani-breeze-dark/tree/1.3.4
[1.3.3]: https://gitlab.com/valeth/wanikani-breeze-dark/tree/1.3.3
[1.3.2]: https://gitlab.com/valeth/wanikani-breeze-dark/tree/1.3.2
[1.3.1]: https://gitlab.com/valeth/wanikani-breeze-dark/tree/1.3.1
[1.3.0]: https://gitlab.com/valeth/wanikani-breeze-dark/tree/1.3.0
[1.2.4]: https://gitlab.com/valeth/wanikani-breeze-dark/tree/1.2.4
[1.2.3]: https://gitlab.com/valeth/wanikani-breeze-dark/tree/1.2.3
[1.2.2]: https://gitlab.com/valeth/wanikani-breeze-dark/tree/1.2.2
[1.2.1]: https://gitlab.com/valeth/wanikani-breeze-dark/tree/1.2.1
[1.2.0]: https://gitlab.com/valeth/wanikani-breeze-dark/tree/1.2.0
[1.1.0]: https://gitlab.com/valeth/wanikani-breeze-dark/tree/1.1.0
[1.0.0]: https://gitlab.com/valeth/wanikani-breeze-dark/tree/1.0.0
[0.6.0]: https://gitlab.com/valeth/wanikani-breeze-dark/tree/0.6.0
[0.1.0]: https://gitlab.com/valeth/wanikani-breeze-dark/tree/0.1.0

